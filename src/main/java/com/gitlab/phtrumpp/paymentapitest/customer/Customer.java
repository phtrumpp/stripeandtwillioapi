package com.gitlab.phtrumpp.paymentapitest.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.NotEmpty;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
//This will ignore anything coming from the client, but when I send the payload to the client it will include the id (allowGetters)
// **Edit, removed id from ignore properties for testing
@JsonIgnoreProperties(allowGetters = true)
@ToString
public class Customer {

    @Id
    private UUID id;

    @NotEmpty
    @Column(nullable = false)
    private String name;

    @NotEmpty
    @Column(nullable = false, unique = true)
    private String phone_Number;

    public Customer() {
    }

    public Customer(UUID id, String name, String phone_Number) {
        this.id = id;
        this.name = name;
        this.phone_Number = phone_Number;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_Number() {
        return phone_Number;
    }

    public void setPhone_Number(String phone_Number) {
        this.phone_Number = phone_Number;
    }
}
