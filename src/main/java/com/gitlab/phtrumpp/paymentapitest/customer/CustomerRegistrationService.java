package com.gitlab.phtrumpp.paymentapitest.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerRegistrationService {


    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerRegistrationService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    public void registerNewCustomer(CustomerRegistrationRequest request) {

        // 1. phone_Number is taken
        // 2. if taken lets check if belongs to same customer
        // 2.1 if yes, return
        // 2.2 otherwise throw exception
        // 3. if everything is fine, then save customer

        String numberToCheck = request.getCustomer().getPhone_Number();

        Optional<Customer> customerOptional = customerRepository.findCustomerByPhone_Number(numberToCheck);

        if (customerOptional.isPresent()){
            Customer customer = customerOptional.get();
            // If its the same customer -> execute method
            if (customer.getName().equals(request.getCustomer().getName())){
                return;
            }

            throw new IllegalStateException(String.format("phone number [%s] is already taken", numberToCheck));
        }

        if(request.getCustomer().getId() == null){
            request.getCustomer().setId(UUID.randomUUID());
        }
        customerRepository.save(request.getCustomer());

    }

}
