package com.gitlab.phtrumpp.paymentapitest.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

@ToString
public class CustomerRegistrationRequest {

    private final Customer customer;

    /**Client request
     *
     * @param customer JsonProperty: a property with the name customer comes from the client
     */
    public CustomerRegistrationRequest(@JsonProperty("customer") Customer customer) {
        this.customer = customer;

    }

    public Customer getCustomer() {
        return customer;
    }
}
