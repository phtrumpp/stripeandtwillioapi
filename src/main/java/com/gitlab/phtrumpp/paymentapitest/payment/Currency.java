package com.gitlab.phtrumpp.paymentapitest.payment;

public enum Currency {
    USD,
    EURO,
    GBP
}
