package com.gitlab.phtrumpp.paymentapitest.payment.stripe;

import com.gitlab.phtrumpp.paymentapitest.payment.CardPaymentCharge;
import com.gitlab.phtrumpp.paymentapitest.payment.CardPaymentCharger;
import com.gitlab.phtrumpp.paymentapitest.payment.Currency;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.net.RequestOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


@Service
@ConditionalOnProperty(
        value = "stripe.enabled",
        havingValue = "true"
)
public class StripeService implements CardPaymentCharger {

    private final StripeApi stripeApi;

    private final static RequestOptions requestOptions = RequestOptions.builder()
            .setApiKey("sk_test_4eC39HqLyjWDarjtT1zdp7dc")
            .build();

    @Autowired
    public StripeService(StripeApi stripeApi) {
        this.stripeApi = stripeApi;
    }


    @Override
    public CardPaymentCharge chargeCard(BigDecimal amount,
                                        Currency currency,
                                        String cardSource, String description) {

        Stripe.apiKey = "sk_test_4eC39HqLyjWDarjtT1zdp7dc";

        // `source` is obtained with Stripe.js; see https://stripe.com/docs/payments/accept-a-payment-charges#web-create-token
        Map<String, Object> params = new HashMap<>();
        params.put("amount", amount);
        params.put("currency", currency);
        params.put("source", cardSource);
        params.put(
                "description", description
        );

        try {
            Charge charge = stripeApi.createAPI(requestOptions, params);// I need to pass requestOptions if I haven't init api key
            Boolean chargePaid = charge.getPaid();

            return new CardPaymentCharge(chargePaid);

        } catch (StripeException e) {
           throw new IllegalStateException("Cannot make stripe charge", e);
        }
    }
}
