package com.gitlab.phtrumpp.paymentapitest.payment;

import com.gitlab.phtrumpp.paymentapitest.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class PaymentService {

    private static final List<Currency> ACCEPTED_CURRENCIES = List.of(Currency.USD,Currency.EURO);

    private final CustomerRepository customerRepository;
    private final PaymentRepository paymentRepository;

    /**
     * At the moment I use Stripe as a paymentservice so this field refers to the StripeService
     */
    private final CardPaymentCharger cardPaymentCharger;

    @Autowired
    public PaymentService(CustomerRepository customerRepository, PaymentRepository paymentRepository, CardPaymentCharger cardPaymentCharger) {
        this.customerRepository = customerRepository;
        this.paymentRepository = paymentRepository;
        this.cardPaymentCharger = cardPaymentCharger;
    }


    void chargeCardService(PaymentRequest paymentRequest, UUID customerId){

        // 1. Does customer exist if not throw
        boolean isCustomerFound = customerRepository.findById(customerId)
                .isPresent();
        if (!isCustomerFound){
            throw new IllegalStateException(String.format("Customer with id [%s] was not found", customerId));
        }

        Payment payment = paymentRequest.getPayment();

        // 2. Do we support the currency if not throw
        boolean isCurrencySupported = ACCEPTED_CURRENCIES.contains(paymentRequest.getPayment().getCurrency());

        if (!isCurrencySupported){
            String message = String.format("Currency [%s] is not supported", payment.getCurrency());
            throw new IllegalStateException(message);
        }

        // 3. Charge card
        CardPaymentCharge card = cardPaymentCharger.chargeCard(
                payment.getAmount(),
                payment.getCurrency(),
                payment.getSource(),
                payment.getDescription());

        // 4. If not debited throw
        if (!card.isCardDebited()){
            throw new IllegalStateException(String.format("Card not debited for customer [%s]", customerId));
        }
        // 5. Insert payment
        payment.setCustomerId(customerId);
        paymentRepository.save(payment);

        // 6. TODO: send sms
    }


}
