package com.gitlab.phtrumpp.paymentapitest.payment.stripe;

import com.gitlab.phtrumpp.paymentapitest.payment.CardPaymentCharge;
import com.gitlab.phtrumpp.paymentapitest.payment.CardPaymentCharger;
import com.gitlab.phtrumpp.paymentapitest.payment.Currency;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;


/**
 * Mocking stripe for testing
 */
@Service
// Spring will enable this bean if the stripe.enabled property in application.properties has the value false
@ConditionalOnProperty(
        value = "stripe.enabled",
        havingValue = "false"
)
public class MockStripeService implements CardPaymentCharger {

    @Override
    public CardPaymentCharge chargeCard(BigDecimal amount, Currency currency, String cardSource, String description) {

        return new CardPaymentCharge(true);

    }
}
