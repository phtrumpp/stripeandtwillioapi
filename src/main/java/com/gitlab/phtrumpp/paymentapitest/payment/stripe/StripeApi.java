package com.gitlab.phtrumpp.paymentapitest.payment.stripe;

import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.net.RequestOptions;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class StripeApi {

    public Charge createAPI(RequestOptions options, Map<String, Object> requestMap) throws StripeException {
        return Charge.create(requestMap, options); // I need to pass requestOptions if I haven't init api key

    }
}
