package com.gitlab.phtrumpp.paymentapitest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentapitestApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaymentapitestApplication.class, args);
    }

}
