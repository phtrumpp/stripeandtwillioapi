package com.gitlab.phtrumpp.paymentapitest.payment;

import com.gitlab.phtrumpp.paymentapitest.customer.Customer;
import com.gitlab.phtrumpp.paymentapitest.customer.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;

class PaymentServiceTest {

    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private PaymentRepository paymentRepository;
    @Mock
    private CardPaymentCharger cardPaymentCharger;

    private PaymentService underTest;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = new PaymentService(customerRepository,paymentRepository,cardPaymentCharger);
    }

    @Test
    void itShouldChargeCardSuccefsully() {

        // Given
        UUID customerId = UUID.randomUUID();

        // Customer exist
        given(customerRepository.findById(customerId)).willReturn(Optional.of(mock(Customer.class)));
        // instead of creating an instance I simply mock a customer

        PaymentRequest paymentRequest = new PaymentRequest(
                new Payment(
                        null,
                        null,
                        new BigDecimal("100.00"),
                        Currency.USD,
                        "card123xx",
                        "Donation"
                )
        );

        // Card is charged succesfully
        given(cardPaymentCharger.chargeCard(
                paymentRequest.getPayment().getAmount(),
                paymentRequest.getPayment().getCurrency(),
                paymentRequest.getPayment().getSource(),
                paymentRequest.getPayment().getDescription())
        ).willReturn(new CardPaymentCharge(true));

        // When
        underTest.chargeCardService(paymentRequest,customerId);
        // Then
        ArgumentCaptor<Payment> paymentArgumentCaptor =
                ArgumentCaptor.forClass(Payment.class);

        then(paymentRepository).should().save(paymentArgumentCaptor.capture());
        Payment paymentArgumentCaptorValue = paymentArgumentCaptor.getValue();

        assertThat(paymentArgumentCaptorValue)
                .isEqualToIgnoringGivenFields(paymentRequest.getPayment(), "customerId");

        assertThat(paymentArgumentCaptorValue.getCustomerId()).isEqualTo(customerId);
    }

    @Test
    void itShouldThrowWhenCardIsNotCharged() {

        // Given
        UUID customerId = UUID.randomUUID();

        // Customer exist
        given(customerRepository.findById(customerId)).willReturn(Optional.of(mock(Customer.class)));
        // instead of creating an instance I simply mock a customer

        PaymentRequest paymentRequest = new PaymentRequest(
                new Payment(
                        null,
                        null,
                        new BigDecimal("100.00"),
                        Currency.USD,
                        "card123xx",
                        "Donation"
                )
        );

        // Card is charged succesfully
        given(cardPaymentCharger.chargeCard(
                paymentRequest.getPayment().getAmount(),
                paymentRequest.getPayment().getCurrency(),
                paymentRequest.getPayment().getSource(),
                paymentRequest.getPayment().getDescription())
        ).willReturn(new CardPaymentCharge(false));


        // Then
        assertThatThrownBy(() -> underTest.chargeCardService(paymentRequest,customerId))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining(String.format("Card not debited for customer [%s]", customerId));

        then(paymentRepository).should(never()).save(any(Payment.class));
    }

    @Test
    void itShouldNotChargeAndThrowWhenCurrencyNotSupported() {

        // Given
        UUID customerId = UUID.randomUUID();

        // Customer exist
        given(customerRepository.findById(customerId)).willReturn(Optional.of(mock(Customer.class)));
        // instead of creating an instance I simply mock a customer

        // When
        PaymentRequest paymentRequest = new PaymentRequest(
                new Payment(
                        null,
                        null,
                        new BigDecimal("100.00"),
                        Currency.GBP,
                        "card123xx",
                        "Donation"
                )
        );

        // Then
        assertThatThrownBy(()-> underTest.chargeCardService(paymentRequest,customerId))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining(
                        String.format("Currency [%s] is not supported", paymentRequest.getPayment().getCurrency()));

        // Given Currency is not supported, card should not be charged!
        then(cardPaymentCharger).shouldHaveNoInteractions();

        then(paymentRepository).should(never()).save(any(Payment.class));
    }

    @Test
    void itShouldThrowWhenCustomerNotFoundAndNotCharge() {

        // Given
        UUID customerId = UUID.randomUUID();

        PaymentRequest paymentRequest = new PaymentRequest(
                new Payment(
                        null,
                        null,
                        new BigDecimal("100.00"),
                        Currency.USD,
                        "card123xx",
                        "Donation"
                )
        );

        // When customer does not exist
        given(customerRepository.findById(customerId)).willReturn(Optional.empty());

        // Then
        assertThatThrownBy(()-> underTest.chargeCardService(paymentRequest,customerId))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining(
                        String.format(String.format("Customer with id [%s] was not found", customerId)));

        // No interaction with paymentCharger and paymentRepo
        then(cardPaymentCharger).shouldHaveNoInteractions();
        then(paymentRepository).shouldHaveNoInteractions();
    }
}
