package com.gitlab.phtrumpp.paymentapitest.payment;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * In order to test against a H2 database I have to use @DataJpaTest annotation
 */
@DataJpaTest(
        properties = {
                "spring.jpa.properties.javax.persistence.validation.mode=none"
        }
)
class PaymentRepositoryTest {

    @Autowired
    private PaymentRepository underTest;

    @Test
    void itShouldInsertPayment() {

        // Given
        long paymentId = 1L;
        Payment payment = new Payment(
                paymentId, UUID.randomUUID(), new BigDecimal("10.00"),
                Currency.USD,"card123","Donation");
        // When
        underTest.save(payment);

        // Then
        assertThat(underTest.findById(paymentId))
                .isPresent()
                .hasValueSatisfying(p -> assertThat(p).isEqualTo(payment));
        // isEqualTo works bc Lombok overrides Hashcode + Equals of Payment Class
    }
}
