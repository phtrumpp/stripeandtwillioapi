package com.gitlab.phtrumpp.paymentapitest.payment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.phtrumpp.paymentapitest.customer.Customer;
import com.gitlab.phtrumpp.paymentapitest.customer.CustomerRegistrationRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest // entire application will start instead of only the test
@AutoConfigureMockMvc
public class PaymentIntegrationTest {


    private final MockMvc mockMvc;
    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentIntegrationTest(MockMvc mockMvc, PaymentRepository paymentRepository) {
        this.mockMvc = mockMvc;
        this.paymentRepository = paymentRepository;
    }
    // To avoid confusion: I don't have another service endpoint yet to retrieve a payment
    // which is why I am using the repo in this particular case


    @Test
    void itShouldCreatePaymentSuccessfully() throws Exception {

        // Given a customer
        UUID customerId = UUID.randomUUID();
        Customer customer = new Customer(customerId,"Philippe","0000000");
        CustomerRegistrationRequest request = new CustomerRegistrationRequest(customer);

        // Register
        ResultActions customerRegResultActions = mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/customer-registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(objectToJson(request))));// should not be null


        // ...PAyment
        long paymentId = 1L;
        Payment payment = new Payment(paymentId, customerId,
                new BigDecimal("100.00"),Currency.EURO,
                "x0x0x0","Beispiel Beschreibung");

        //... Paymentrequest
        PaymentRequest paymentRequest = new PaymentRequest(payment);


        // When payment is sent
        ResultActions paymentResultActions = mockMvc.perform(post("/api/v1/payment")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(objectToJson(paymentRequest)))
        );


        // Then both customer registration and payment requests are 200 status
        customerRegResultActions.andExpect(status().isOk());

        paymentResultActions.andExpect(status().isOk());

        // payment is stored in db
        // Todo: Do not use paymentRepository instead create an endpoint to retrieve payments for customers
        assertThat(paymentRepository.findById(paymentId))
                .isPresent()
                .hasValueSatisfying(p -> assertThat(p).isEqualToComparingFieldByField(payment));

        // ToDo: Ensure sms is delivered

    }

    /**
     * Uses toString of an object to create a String value into JSON
     * @param object any type for mapping to JSON
     * @return JSON object
     */
    private String objectToJson(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            fail("Failed to convert object to json");
            return null;
        }
    }
}
