package com.gitlab.phtrumpp.paymentapitest.payment.stripe;


import com.gitlab.phtrumpp.paymentapitest.payment.CardPaymentCharge;
import com.gitlab.phtrumpp.paymentapitest.payment.Currency;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.net.RequestOptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

class StripeServiceTest {

    private StripeService underTest;

    @Mock
    private StripeApi stripeApi;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = new StripeService(stripeApi);
    }

    @Test
    void itShouldChargeCard() throws StripeException {

        // Given
        String cardSource = "0x0x0x";
        String description = "Zakat";
        BigDecimal amount = new BigDecimal("10.00");
        Currency currency = Currency.USD;

        Charge charge = new Charge();
        charge.setPaid(true);
        given(stripeApi.createAPI(any(),anyMap())).willReturn(charge); //to avoid nullp. exception

        // When
        CardPaymentCharge cardPaymentCharge = underTest.chargeCard(amount, currency, cardSource, description);

        // Then
        ArgumentCaptor<Map<String, Object>> mapArgumentCaptor = ArgumentCaptor.forClass(Map.class);
        ArgumentCaptor<RequestOptions> optionsArgumentCaptor = ArgumentCaptor.forClass(RequestOptions.class);

        then(stripeApi).should().createAPI(optionsArgumentCaptor.capture(),mapArgumentCaptor.capture());

        Map<String, Object> requestMap = mapArgumentCaptor.getValue();

        assertThat(requestMap.keySet()).hasSize(4);
        assertThat(requestMap.get("amount")).isEqualTo(amount);
        assertThat(requestMap.get("currency")).isEqualTo(currency);
        assertThat(requestMap.get("source")).isEqualTo(cardSource);
        assertThat(requestMap.get("description")).isEqualTo(description);

        RequestOptions options = optionsArgumentCaptor.getValue();

        assertThat(options).isNotNull(); //

        assertThat(cardPaymentCharge.isCardDebited()).isTrue();


    }

    @Test
    void itShouldThrowAndNotCharge() {
        // Given
        String cardSource = "0x0x0x";
        String description = "Zakat";
        BigDecimal amount = new BigDecimal("10.00");
        Currency currency = Currency.USD;

        Charge charge = new Charge();
        charge.setPaid(false);
//        given(stripeApi.createAPI(any(),anyMap())).willReturn(charge); //to avoid nullp. exception
        // When
        // Then
    }
}
