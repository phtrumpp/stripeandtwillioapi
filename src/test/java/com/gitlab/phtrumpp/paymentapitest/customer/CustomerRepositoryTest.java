package com.gitlab.phtrumpp.paymentapitest.customer;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * In order to test against a H2 database I have to use @DataJpaTest annotation
 */
@DataJpaTest(
        properties = {
                "spring.jpa.properties.javax.persistence.validation.mode=none"
        }
)
class CustomerRepositoryTest {

    /**
     * shift strg(cmd) + t -> shortcut coming back to the class/interface which is tested
     */

    @Autowired
    private CustomerRepository underTest;

    // TestMethod - edit Template
    @Test
    void itShouldSelectCustomerByPhoneNumber() {

        // Given
        Customer customer = new Customer(UUID.randomUUID(),"Phil","1111");

        // When
        underTest.save(customer);

        // Then
        assertThat(underTest.findCustomerByPhone_Number("1111"))
                .isPresent()
                .hasValueSatisfying(customerDB -> assertThat(customerDB)
                .isEqualToComparingFieldByField(customer));
    }

    @Test
    void itShouldNotSelectCustomerByPhoneNumberWhenNumberDoesNotExist() {

        // Given
        String phoneNumber = "1001";
        // When
        Optional<Customer> optionalCustomer = underTest.findCustomerByPhone_Number(phoneNumber);
        // Then
        assertThat(optionalCustomer)
                .isNotPresent();
    }

    @Test
    void itShouldSaveCustomer() {


        // Given
        UUID id = UUID.randomUUID();
        Customer phil = new Customer(id, "Phil", "0000");
        // When
        underTest.save(phil);
        // Then
        Optional<Customer> optionalCustomer = underTest.findById(id);

        assertThat(optionalCustomer)
                .isPresent()
                .hasValueSatisfying(c ->{
//                    assertThat(c.getId()).isEqualTo(id);
//                    assertThat(c.getName()).isEqualTo("Phil");
//                    assertThat(c.getPhone_Number()).isEqualTo("0001");
                });
        assertThat(optionalCustomer)
                .isPresent()
                .hasValueSatisfying(c -> assertThat(c).isEqualToComparingFieldByField(phil));

    }

    @Test
    void itShouldNotSaveCustomerWhenNameIsNull() {

        // Given
        UUID id = UUID.randomUUID();
        Customer phil = new Customer(id, null, "0000");

        // When
        // Then
        assertThatThrownBy(() -> underTest.save(phil))
                .hasMessageContaining("not-null property references a null or transient value : com.gitlab.phtrumpp.paymentapitest.customer.Customer.name")
                .isInstanceOf(DataIntegrityViolationException.class);

    }

    @Test
    void itShouldNotSaveCustomerWhenPhoneNumberIsNull() {

        // Given
        UUID id = UUID.randomUUID();
        Customer phil = new Customer(id, "Phil", null);

        // When
        // Then
        assertThatThrownBy(() -> underTest.save(phil))
                .hasMessageContaining("not-null property references a null or transient value : com.gitlab.phtrumpp.paymentapitest.customer.Customer.phone_Number")
                .isInstanceOf(DataIntegrityViolationException.class);
    }
}
