package com.gitlab.phtrumpp.paymentapitest.customer;


import com.gitlab.phtrumpp.paymentapitest.payment.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;

class CustomerRegistrationServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @Captor
    private ArgumentCaptor<Customer> customerArgumentCaptor;

    private CustomerRegistrationService underTest;

    @BeforeEach
    void setUp() {
        //to initialize the actual mock.
        // other way would be:
        // private CustomerRepository customerRepository = mock (CustomerRepository.class)
        MockitoAnnotations.initMocks(this);
        underTest = new CustomerRegistrationService(customerRepository);
    }

    @Test
    void itShouldSaveNewCustomer() {

        // Given
        String phoneNumber = "0000";
        Customer customer = new Customer(UUID.randomUUID(),"Nine", phoneNumber);

        // ... a request
        CustomerRegistrationRequest request = new CustomerRegistrationRequest(customer);

        // ... No customer with phone number passed
        // Tell that I am expecting no result
        given(customerRepository.findCustomerByPhone_Number(phoneNumber))
                .willReturn(Optional.empty());

        // When
        underTest.registerNewCustomer(request);

        // Then
        then(customerRepository).should().save(customerArgumentCaptor.capture()); // the argument catpor captures the value(customer) of the customerRepository.save
        Customer customerArgumentCaptorValue = customerArgumentCaptor.getValue();

        assertThat(customerArgumentCaptorValue).isEqualToComparingFieldByField(customer);
    }

    @Test
    void itShouldNotSaveCustomerWhenCustomerExists() {

        // Given
        String phoneNumber = "0000";
        UUID id = UUID.randomUUID();
        Customer customer = new Customer(id,"Nine", phoneNumber);

        // ... a request
        CustomerRegistrationRequest request = new CustomerRegistrationRequest(customer);

        // ... an existing customer is returned
        // Tell that I am expecting a result
        given(customerRepository.findCustomerByPhone_Number(phoneNumber))
                .willReturn(Optional.of(customer));
        // When
        underTest.registerNewCustomer(request);
        // Then
        then(customerRepository).should(never()).save(any());
       // then(customerRepository).shouldHaveNoInteractions();   -- is also possible
    }

    @Test
    void itShouldThrowExceptionWhenNumberTaken() {

        // Given
        // Given
        String phoneNumber = "0000";

        Customer customer = new Customer(UUID.randomUUID(),"Nine", phoneNumber);
        Customer customer2 = new Customer(UUID.randomUUID(),"Cube", phoneNumber);

        // ... a request
        CustomerRegistrationRequest request = new CustomerRegistrationRequest(customer);

        // ... an existing customer is returned
        // Tell that I am expecting no result - Given a mocked bean finds something
        given(customerRepository.findCustomerByPhone_Number(phoneNumber))
                .willReturn(Optional.of(customer2));

        // When
        // Then
        assertThatThrownBy(() -> underTest.registerNewCustomer(request))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining(String.format("phone number [%s] is already taken", phoneNumber));

        // Finally
        then(customerRepository).should(never()).save(any());

    }

    @Test
    void itShouldSaveNewCustomerWhenIdIsNull() {

        // Given
        String phoneNumber = "0000";

        Customer customer = new Customer(null,"Nine", phoneNumber); // This time id is null

        // ... a request
        CustomerRegistrationRequest request = new CustomerRegistrationRequest(customer);

        // ... No customer with phone number passed
        // Tell that I am expecting no result
        given(customerRepository.findCustomerByPhone_Number(phoneNumber))
                .willReturn(Optional.empty());

        // When
        underTest.registerNewCustomer(request);

        // Then
        then(customerRepository).should().save(customerArgumentCaptor.capture()); // captures again the value of customer ( and itself generated id, see if condition)
        Customer customerArgumentCaptorValue = customerArgumentCaptor.getValue();

        //
        assertThat(customerArgumentCaptorValue).isEqualToIgnoringGivenFields(customer,"id");

        assertThat(customerArgumentCaptorValue.getId()).isNotNull();
    }


}
